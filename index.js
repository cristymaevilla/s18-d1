/* OBJECTS: objectnamme={property,property}
	Syntax:
		let ogjectName ={
			keyA: valueA,
			keyB: valueB
		}
}

*/
let cellphone = {
		name: 'Nokia 3210',
		manufactureDate: 1999
}
console.log('Result from creating objects using unitializers/ literal notation:')
// TO ACCESS OJECTS:----------------------------
console.log(cellphone);
console.log( "Using typeof to know what kind is cellphone: "+ typeof cellphone);
console.log(cellphone.name)

// CREATING OBJECTS USING CONSTRUCTOR FUNCTION:-------------------------------------------------
/* - creates a reusable function to create several objects that have the same data structure
	SYNTAX:
		function objectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
			}
	"this"-it allows us to assign a new object's properties by associating them with values received from a constructors function's parameters
*/
function Laptop(name, manufactureDate){
	this.name=name;
	this.manufactureDate=manufactureDate;
}
let laptop = new Laptop('Lenovo' , 2008);
console.log('result from creating objects using objects constructors:')
console.log (laptop);

let myLaptop= new Laptop('MacBook Air', 2020);
console.log('result from reassigning objects using objects constructors:')
console.log (myLaptop);

let herLaptop= new Laptop('Dell', 2019);
console.log('result from reassigning objects using objects constructors:')
console.log (herLaptop);


console.log("ACCESSING SPECIFIC OBJECT PROPERTIES");
// ACCESSING SPECIFIC OBJECT PROPERTIES-------------------
// DOT NOTATION
console.log("Result from DOT notation:" + myLaptop.name);

// SQUARE BRACKET - case sensitive
console.log("Result from square bracket notation:" + myLaptop['name']);

// ACCESSING ARRAY PROPERTIES------------------------
let arr = [laptop, myLaptop];

console.log(arr[0]);
// ACCESSING specific  PROPERTIES of an object----------------------------------
console.log(arr[0].name);
console.log(arr[0]['name']);

// Initializing/deleting/adding/Reassigning Object Properties
// ADDING property to your empty object
let car={}
car.name="Honda Civic";
console.log("Result from adding properties using DOT notation");
console.log(car);

// you can add space inside square bracket
car['manufacture date']=2019;

console.log(car ['manufacture date']);
// below is an error because property is not the same with above mentioned
console.log(car ['Manufacture Date']);


console.log(car);
// DELETING object property
delete car ['manufacture date'];
console.log("Result from DELETING properties using bracket method");
console.log(car);

// REASSIGNING object property 
car.name= "E46 BMW M3 GTR"
console.log("Result from REASSIGNNING properties using dot method");
console.log(car);

// OBEJECT METHOD:===============================
/* method - a fucntion w/c is a property of an object
*/
console.log("OBJECT METHOD");

let person = {
	name: 'John',
	talk: function(){
		console.log("Hello my name is "+ this.name);
	}
}
// note: this.name (see above) to access "John"
console.log(person);

// invoke the function inside talk property:-------
// objectName.propertyName()
console.log("invoke function in the talk property: objectName.propertyName() ");
person.talk();

// ADD another property with method or function
console.log("ADD another property with method or function ")
person.walk =function(){
	console.log(this.name + " walked 25 steps backward.")
}
person.walk();

person.run =function(){
	console.log(this.name + " runs as fast as lightning.")
}
person.run();

// CREATING REAUSABLE functions
console.log("CREATING REAUSABLE functions ")
let friend ={
	firstName: "Joe",
	lastName: "Smith",
	address:{
		city: "Austin",
		country: "Texas"
	},
	emails: ["joe@mail.com" ,"joesmith@email.css"],
	introduce: function(){
		console.log("Hello my name is "+ this.firstName + " " + this.lastName);
	}
}
friend.introduce();
console.log(friend.firstName, friend.lastName);
console.log(friend.firstName, friend.emails);
console.log(friend.emails[1]);


// REAL WORLD APPLICATION of OBJECTS=====================
console.log("REAL WORLD APPLICATION of OBJECTS")
/*SCENARIO:
	1. Create a game that have several pokemon interact w/ each other
	2. Every pokemon would have the same set of stats, properties and functions
	*/
let myPokemon={
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 100,
	tackle: function(){
		console.log ("This Pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");		
		},
	faint: function (){
		console.log("Pokemon fainted");
	}
}
console.log(myPokemon);

// POKEMON OBJECT CONSTRUCTOR
function Pokemon(name,level){
	// Properties
	this.name=name;
	this.level=level;
	this.health= 2*level;
	this.attack= level;

	// Methods
	this.tackle=function(target){
		console.log( this.name + " tackled " + target.name);
		let attackResult =target.health - this.attack;
		console.log(target.name +"'s health is now reduced to " + attackResult );
	};
	this.faint =function(target){
		let targetHealth =target.health - this.attack;
		let myPokemontHealth =this.health - this.attack;

		if ( targetHealth ==>0 && myPokemontHealth>0){
			console.log(target.name + " has fainted. ");
			console.log(this.name + "'s' remaining health is " + myPokemontHealth ".");
		};
		if ( targetHealth>0 && myPokemontHealth ==>0){
			 console.log(target.name + " has won. Try again next time. ");
		};
		if ( targetHealth>0 && myPokemontHealth>0){
			console.log(target.name + " is still alive");
			console.log(this.name + "'s' remaining health is " + myPokemontHealth ".");
		};
		else ( targetHealth==>0 && myPokemontHealth ==>0){
			 console.log(" It's a draw ");
		};

	};
	
}
// create NEW instances of the "Pokemon" object each with their unique properties:

let pikachu= new Pokemon ("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.faint(rattata)